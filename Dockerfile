FROM openjdk:8
MAINTAINER Vijay Sekhri
RUN mkdir -p /tmp/cq/aem/data/datastore
RUN mkdir -p /opt/cq/aem
ADD *.jar /tmp/cq/aem/
ADD license.properties /tmp/cq/aem/
ADD org.apache.jackrabbit.oak.plugins.blob.datastore.FileDataStore.config /tmp/cq/aem/
ADD org.apache.jackrabbit.oak.segment.SegmentNodeStoreService.config /tmp/cq/aem/
COPY crx-quickstart /tmp/cq/aem/crx-quickstart
RUN ls -la /tmp/cq/aem/*
#WORKDIR /opt/cq/aem

EXPOSE 4502

RUN apt-get update
RUN apt-get install inotify-tools -y
ADD entrypoint.sh /entrypoint.sh
ENV PATH /:$PATH
RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]


#CMD /opt/cq/aem/crx-quickstart/bin/quickstart
