#!/bin/bash

#set -e


echo "ls -lrth /opt/cq/aem"
ls -lrth /opt/cq/aem

echo "mkdir -p /opt/cq/aem/crx-quickstart/logs/"
mkdir -p /opt/cq/aem/crx-quickstart/logs/

echo "mkdir -p  /opt/cq/aem/crx-quickstart/conf/"
mkdir -p /opt/cq/aem/crx-quickstart/conf/

echo "mkdir -p /opt/cq/aem/crx-quickstart/app/"
mkdir -p /opt/cq/aem/crx-quickstart/app/

echo "mkdir -p  /opt/cq/aem/crx-quickstart/bin/"
mkdir -p /opt/cq/aem/crx-quickstart/bin/

echo "mkdir -p  /opt/cq/aem/crx-quickstart/install/"
mkdir -p /opt/cq/aem/crx-quickstart/install/

echo "mkdir -p /opt/cq/aem/temp/"
mkdir -p /opt/cq/aem/temp/



echo "cp -r /tmp/cq/aem/crx-quickstart/app/* /opt/cq/aem/crx-quickstart/app/"
cp -r /tmp/cq/aem/crx-quickstart/app/* /opt/cq/aem/crx-quickstart/app/

echo "cp -r /tmp/cq/aem/crx-quickstart/bin/* /opt/cq/aem/crx-quickstart/bin/"
cp -r /tmp/cq/aem/crx-quickstart/bin/* /opt/cq/aem/crx-quickstart/bin/

echo "cp -r /tmp/cq/aem/crx-quickstart/opt /opt/cq/aem/crx-quickstart/"
cp -r /tmp/cq/aem/crx-quickstart/opt /opt/cq/aem/crx-quickstart/


echo "cp /tmp/cq/aem/crx-quickstart/*html /opt/cq/aem/crx-quickstart/"
cp /tmp/cq/aem/crx-quickstart/*html /opt/cq/aem/crx-quickstart/


echo "cp /tmp/cq/aem/crx-quickstart/*txt /opt/cq/aem/crx-quickstart/"
cp /tmp/cq/aem/crx-quickstart/*txt /opt/cq/aem/crx-quickstart/


echo "cp -r /tmp/cq/aem/*.jar /opt/cq/aem/"
cp -r /tmp/cq/aem/*.jar /opt/cq/aem/


echo "cp -r /tmp/cq/aem/license.properties /opt/cq/aem/"
cp -r /tmp/cq/aem/license.properties /opt/cq/aem/


echo "cp -r /tmp/cq/aem/org.apache.jackrabbit.oak.plugins.blob.datastore.FileDataStore.config /opt/cq/aem/crx-quickstart/install/"
cp -r /tmp/cq/aem/org.apache.jackrabbit.oak.plugins.blob.datastore.FileDataStore.config /opt/cq/aem/crx-quickstart/install/

echo "cp -r /tmp/cq/aem/org.apache.jackrabbit.oak.segment.SegmentNodeStoreService.config /opt/cq/aem/crx-quickstart/install/"
cp -r /tmp/cq/aem/org.apache.jackrabbit.oak.segment.SegmentNodeStoreService.config /opt/cq/aem/crx-quickstart/install/


echo "cd /opt/cq/aem"
cd /opt/cq/aem

#confFile=/opt/cq/aem/crx-quickstart/conf/bundles.json
#if [ -f $FILE ]; then
#	echo "File ${confFile} exists."
#else
#	echo "File ${confFile} does not exist."
#	echo "cp -r /tmp/cq/aem/crx-quickstart/conf/bundles.json /opt/cq/aem/crx-quickstart/conf/"
#	cp -r /tmp/cq/aem/crx-quickstart/conf/bundles.json /opt/cq/aem/crx-quickstart/conf/
#fi

echo "touch /opt/cq/aem/crx-quickstart/logs/upgrade.log"
touch /opt/cq/aem/crx-quickstart/logs/upgrade.log

echo "ls -lrth /opt/cq/aem/crx-quickstart/bin/"
ls -lrth /opt/cq/aem/crx-quickstart/bin/

echo "chmod 777 /opt/cq/aem/crx-quickstart/bin/quickstart"
chmod 777 /opt/cq/aem/crx-quickstart/bin/quickstart

echo "chmod 777 /opt/cq/aem/crx-quickstart/bin/start"
chmod 777 /opt/cq/aem/crx-quickstart/bin/start


#echo "/opt/cq/aem/crx-quickstart/bin/quickstart"
#/opt/cq/aem/crx-quickstart/bin/quickstart

echo "/opt/cq/aem/crx-quickstart/bin/start"
/opt/cq/aem/crx-quickstart/bin/start


echo  "Listing iles in /opt/cq/aem/crx-quickstart/logs/"
ls -lrth /opt/cq/aem/crx-quickstart/logs/

echo "Tailing log files"
tail -f /opt/cq/aem/crx-quickstart/logs/* &


inotifywait -m /opt/cq/aem/crx-quickstart/logs -e create -e moved_to |
    while read path action file; do
        echo "The file '$file' appeared in directory '$path' via '$action'"
	echo  "Killing `ps -aef | grep tail |  grep -v grep  | awk '{ print $2 }'`"
        kill -9  `ps -aef | grep "tail -f" |  grep -v grep  | awk '{ print $2 }'`
	echo "Stating tail again "
        tail -f /opt/cq/aem/crx-quickstart/logs/* &
	echo "waiting for new file "
    done

echo "Should not reach here "
cat


