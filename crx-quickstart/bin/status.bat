@echo off
setlocal

:: This script returns the status of the (running) application
:: 0 - if the application is running
:: 1 - if the program is dead
:: 3 - if the program is not running
:: 4 - if there is a unknown problem

chdir /D %~dp0
cd ..
set START_OPTS=status -c .
if not defined CQ_JARFILE     for %%X in (app\*.jar) do set CQ_JARFILE=%%X

java -jar %CQ_JARFILE% %START_OPTS%

set java_exit_code=%ERRORLEVEL%

if %java_exit_code% NEQ 0 goto end
::echo OK

:end
:: exit %java_exit_code%