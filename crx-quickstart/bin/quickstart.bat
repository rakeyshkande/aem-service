@echo off
:: This script configures the start information for this server.
::
:: The following variables may be used to override the defaults.
:: For one-time overrides the variable can be set as part of the command-line; e.g.,
::
::     SET CQ_PORT=1234 & ./start.bat
::
setlocal

::* TCP port used for stop and status scripts
set CQ_PORT=4502

::* hostname of the interface that this server should listen to
:: set CQ_HOST=

::* show gui
set CQ_GUI=true

::* do not show browser on startup
set CQ_NOBROWSER=true

::* do not redirect stdout/stderr (logs to console)
set CQ_VERBOSE=true

::* do not fork the JVM
:: set CQ_NOFORK=true

::* force forking the VM using recommended default memory settings
:: set CQ_FORK=true

::* additional arguments for the forked JVM
:: set CQ_FORKARGS=

::* runmode(s)
:: set CQ_RUNMODE=

::* defines the path under which the quickstart work folder is located
:: set CQ_BASEFOLDER=

::* low memory action
:: set CQ_LOWMEMACTION=

::* name of the jarfile
:: set CQ_JARFILE=

::* use jaas.config
:: set CQ_USE_JAAS=true

::* config for jaas
set CQ_JAAS_CONFIG=etc\jaas.config

::* default JVM options
set CQ_JVM_OPTS=-Xmx1024m -XX:MaxPermSize=256M

::* ------------------------------------------------------------------------------
::* do not configure below this point
::* ------------------------------------------------------------------------------

chdir /D %~dp0
cd ..\..
set START_OPTS=-use-control-port
if defined CQ_PORT            set START_OPTS=%START_OPTS% -p %CQ_PORT%
if defined CQ_GUI             set START_OPTS=%START_OPTS% -gui
if defined CQ_NOBROWSER       set START_OPTS=%START_OPTS% -nobrowser
if defined CQ_VERBOSE         set START_OPTS=%START_OPTS% -verbose
if defined CQ_NOFORK          set START_OPTS=%START_OPTS% -nofork
if defined CQ_FORK            set START_OPTS=%START_OPTS% -fork
if defined CQ_FORKARGS        set START_OPTS=%START_OPTS% -forkargs %CQ_FORKARGS%
if defined CQ_RUNMODE         set START_OPTS=%START_OPTS% -r %CQ_RUNMODE%
if defined CQ_BASEFOLDER      set START_OPTS=%START_OPTS% -b %CQ_BASEFOLDER%
if defined CQ_LOWMEMACTION    set START_OPTS=%START_OPTS% -low-mem-action %CQ_LOWMEMACTION%
if defined CQ_HOST            set CQ_JVM_OPTS=%CQ_JVM_OPTS% -Dorg.apache.felix.http.host=%CQ_HOST%
if defined CQ_HOST            set START_OPTS=%START_OPTS% -a %CQ_HOST%
if defined CQ_USE_JAAS        set CQ_JVM_OPTS=%CQ_JVM_OPTS% -Djava.security.auth.login.config=%CQ_JAAS_CONFIG%
if not defined CQ_JARFILE     for %%X in (*.jar) do set CQ_JARFILE=%%X

tasklist > oldTaskList.txt
start "CQ" cmd.exe /K java %CQ_JVM_OPTS% -jar %CQ_JARFILE% %START_OPTS%
tasklist > newTaskList.txt
java -cp %~dp0 GetProcessID oldTaskList.txt newTaskList.txt java.exe > crx-quickstart\conf\cq.pid
del newTaskList.txt
del oldTaskList.txt