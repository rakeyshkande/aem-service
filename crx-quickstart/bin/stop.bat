@echo off
setlocal

:: This script stops the running application

chdir /D %~dp0
cd ..
set START_OPTS=stop -c .
if not defined CQ_JARFILE     for %%X in (app\*.jar) do set CQ_JARFILE=%%X

java -jar %CQ_JARFILE% %START_OPTS%

set exit_code=%ERRORLEVEL%

if %exit_code% NEQ 0 goto kill_with_pid
echo OK
goto end

:kill_with_pid
set filename=conf\cq.pid
set /p pid=<%filename%
taskkill /PID %pid% /f
set exit_code=%ERRORLEVEL%
del %filename%
goto end

:end