@echo off
setlocal

:: This script configures the windows service start information for this server.
::
:: The following variables may be used to override the defaults.

::* TCP port used for stop and status scripts
set CQ_PORT=4502

::* interface that this server should listen to (ip or hostname)
:: set CQ_HOST=

::* show gui
:: set CQ_GUI=true

::* do not show browser on startup
set CQ_NOBROWSER=true

::* do not redirect stdout/stderr (logs to console)
:: set CQ_VERBOSE=true

::* do not fork the JVM
set CQ_NOFORK=true

::* force forking the VM using recommended default memory settings
:: set CQ_FORK=true

::* additional arguments for the forked JVM
:: set CQ_FORKARGS=

::* runmode(s)
:: set CQ_RUNMODE=

::* defines the path under which the quickstart work folder is located
:: set CQ_BASEFOLDER=

::* low memory action
:: set CQ_LOWMEMACTION=

::* name of the jarfile
:: set CQ_JARFILE=

::* use jaas.config
:: set CQ_USE_JAAS=true

::* config for jaas
set CQ_JAAS_CONFIG=etc\jaas.config

::* default JVM options
set CQ_JVM_OPTS=-Xmx2048m -XX:MaxPermSize=256M

::* Uncomment, or redefine one of the follwoing JAVA_HOME if you wish a
::  different than your default one
::
::  set JAVA_HOME=c:\java\jdk1.4.2_08
::  set JAVA_HOME=%JAVA_HOME%

::* ------------------------------------------------------------------------------
::* do not configure below this point
::* ------------------------------------------------------------------------------

cd ..
set START_OPTS=-use-control-port
if defined CQ_PORT            set START_OPTS=%START_OPTS% -p %CQ_PORT%
if defined CQ_GUI             set START_OPTS=%START_OPTS% -gui
if defined CQ_NOBROWSER       set START_OPTS=%START_OPTS% -nobrowser
if defined CQ_VERBOSE         set START_OPTS=%START_OPTS% -verbose
if defined CQ_NOFORK          set START_OPTS=%START_OPTS% -nofork
if defined CQ_FORK            set START_OPTS=%START_OPTS% -fork
if defined CQ_FORKARGS        set START_OPTS=%START_OPTS% -forkargs %CQ_FORKARGS%
if defined CQ_RUNMODE         set START_OPTS=%START_OPTS% -r %CQ_RUNMODE%
if defined CQ_BASEFOLDER      set START_OPTS=%START_OPTS% -b %CQ_BASEFOLDER%
if defined CQ_LOWMEMACTION    set START_OPTS=%START_OPTS% -low-mem-action %CQ_LOWMEMACTION%
if defined CQ_HOST            set CQ_JVM_OPTS=%CQ_JVM_OPTS% -Dorg.apache.felix.http.host=%CQ_HOST%
if defined CQ_HOST            set START_OPTS=%START_OPTS% -a %CQ_HOST%
if defined CQ_USE_JAAS        set CQ_JVM_OPTS=%CQ_JVM_OPTS% -Djava.security.auth.login.config=%CQ_JAAS_CONFIG%
if not defined CQ_JARFILE     for %%X in (*.jar) do set CQ_JARFILE=%%X

:: check for JVM
if not defined JAVA_HOME (
    :: try to locate a VM in the path
    call :which java.exe JAVA
) else (
    set JAVA="%JAVA_HOME%\bin\java.exe"
)

:: if no VM could be located on the path and no JAVA_HOME was set, stop
if %JAVA%=="" (
    echo JAVA_HOME not specified and no java.exe in PATH
    goto exit
)
if not exist %JAVA% (
    echo No JVM found at %JAVA%
    goto exit
)


%JAVA% %CQ_JVM_OPTS% -jar %CQ_JARFILE% %START_OPTS%


echo -------------------------------------------------------------------------------
echo Starting...
echo -------------------------------------------------------------------------------
call %JAVA% %JVM_TYPE% -version
echo -------------------------------------------------------------------------------
echo %JAVA% %JAVA_VM_TYPE% %JVM_OPTS% -jar %CQ_RUNNABLE_JAR% %CQ_ARGS%
echo -------------------------------------------------------------------------------
:startcq
%JAVA% %JAVA_VM_TYPE% %JVM_OPTS% -jar "%CQ_RUNNABLE_JAR%" %CQ_ARGS%
goto exit

::-----------------------------------------------------------------------------
:which %file% variable
setlocal
set res="%~$PATH:1"
endlocal&set %2=%res%&goto :EOF

::-----------------------------------------------------------------------------
:exit
endlocal
