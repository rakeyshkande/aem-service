@echo off
setlocal

:: service name
set service_name=AEM

:: service description
set service_description=Adobe Experience Manager

:: service start mode <auto|manual>
set service_startmode="auto"

:: path to a jvm.dll
if not defined JAVA_HOME (
    echo Exiting: the environment variable JAVA_HOME needs to be defined
    goto exit
)
set jvm_path="%JAVA_HOME%\jre\bin\server\jvm.dll"
if not exist %jvm_path% (
    echo Exiting: jvm.dll not found at %jvm_path%
    echo Please verify the correct value of JAVA_HOME
    goto exit
)

:: default action is install service
set action=install

:: runmode(s)
set cq_runmode="author"

:: HTTP port
set cq_port=4502

:: interface that this server should listen to (ip or hostname)
:: set cq_host=

:: control port
set cq_ctrlport=63000

:: default context is relative to this installation
set context="%~dp0\..\.."

:: maximum memory pool size
set jvm_mx=2048m

:: default JVM options
:: separate multiple entries by ";" or "#"
:: if you need these chars put them inside single quotes
set jvm_options=-Xmx2048m#-XX:MaxPermSize=256M

:: name of the Apache Commons Daemon Binary
:: if using ia64 architecture, change value to prunsrv_ia64.exe
set PRUNSRV_EXE=prunsrv.exe

::* ------------------------------------------------------------------------------
::* do not configure below this point
::* ------------------------------------------------------------------------------
set main_class=org.apache.sling.launchpad.app.Main
set start_param=start#-c#%context%#-i#%context%\launchpad#-Dsling.properties=%context%\conf\sling.properties
if defined cq_runmode 	(set jvm_options=%jvm_options%#-Dsling.run.modes=%cq_runmode%)
if defined cq_port 	(set start_param=%start_param%#-p#%cq_port%)
if defined cq_ctrlport 	(set start_param=%start_param%#-j#%cq_ctrlport%)
if defined cq_host (set jvm_options=%jvm_options%#-Dorg.apache.felix.http.host=%cq_host%)
if defined cq_host (set start_param=%start_param%#-a#%cq_host%)
set stop_param=stop

:: parse arguments
:while_args
    if "%1"=="-uninstall" (
        set action=uninstall
        goto next_arg
    )
    if "%1"=="-quiet" (
        set quiet=1
        goto next_arg
    )
    if "%1"=="-start" (
        set start=1
        goto next_arg
    )
    if "%1"=="-help" (
        goto usage
    )
    if "%1"=="-context" (
        set context=%~f2
        shift /1
        goto next_arg
    )
    if "%1" NEQ "" set service_name=%1

:next_arg
    shift /1
    if "%1" NEQ "" goto while_args

:: service name must be given, regardless of action
if not defined service_name (
    goto usage
)
for %%X in (%context%\app\*.jar) do set CQ_JARFILE=%%X


if "%action%"=="install" goto install


:uninstall
if defined quiet goto run_uninstall
echo Uninstalling service %service_name%
echo.

:run_uninstall
chdir /D %~dp0
%PRUNSRV_EXE% //DS//%service_name%
goto exit


:install

:: Context directory must be valid, expand it to a full path
call :fullpath %context% context
if not exist "%context%\opt\helpers\start-service.bat" (
    echo Server start script not found: %context%\opt\helpers\start-service.bat
    goto exit
)
set workdir=%context%\..
call :fullpath %workdir% workdir

:install_info
if defined quiet goto run_install
echo Installing service %service_name%
echo     runs in   "%workdir%"
echo     uses      "%CQ_JARFILE%"
echo     starts    %main_class%
echo     redirects output to: %context%\logs\startup.log
echo.

:run_install
chdir /D %~dp0
%PRUNSRV_EXE% //IS//%service_name% --Description="%service_description%" --DisplayName="%service_name%" --Startup="%service_startmode%" --StartPath="%workdir%" --Classpath="%CQ_JARFILE%" --JvmMx=%jvm_mx% --JvmOptions=%jvm_options% --StdOutput="%context%\logs\startup.log" --StdError="%context%\logs\startup.log" --LogPath="%context%\logs" --PidFile=conf\cq.pid --StartMode jvm --StartClass=%main_class% --StartParams=%start_param% --StopMode=jvm --StopClass=%main_class% --StopParams=%stop_param% --Jvm=%jvm_path%
if defined start (net start %service_name%)
goto exit

:usage
echo Usage: %0 [-context ...] [-quiet] [-start] service-name
echo        (to install a service)
echo    or: %0 -uninstall [-quiet] service-name
echo        (to uninstall a service)
goto exit

:fullpath %in% out
setlocal
set res=%~f1
endlocal&set %2=%res%&goto :EOF

:which %file% variable
setlocal
set res="%~$PATH:1"
endlocal&set %2=%res%&goto :EOF

:exit
endlocal

